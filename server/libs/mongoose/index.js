'use strict';
const mongoose = require('mongoose');
const config = require('../../../config');
const log = require('../logger');

mongoose.connect(config.dbName);
const db = mongoose.connection;

db.on('error', err => log.error(`Connection error: ${err.message}`));

db.once('open', () => log.info('Connected to DB!'));

module.exports = db;
