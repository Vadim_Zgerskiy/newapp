'use strict';
const express = require('express');
const config = require('../config');
const log = require('./libs/logger');
require('./libs/mongoose');
const bodyParser = require('body-parser');
const userApi = require('./routers/user');
const app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(userApi);

app.use((err, req, res, next) => res.status(err.status ? err.status : 500).json(err));

app.listen(config.port, () => log.info(`Server listen on port ${config.port}`));

module.exports = app;
