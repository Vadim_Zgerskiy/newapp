'use strict';
module.exports = {
  port: 3000,
  loggerFileName: 'LogInfo.log',
  dbName: 'mongodb://localhost/ourDB',
  secretKey: 'secretKeyForPassword'
};
